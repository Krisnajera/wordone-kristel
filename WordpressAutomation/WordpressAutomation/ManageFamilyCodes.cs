﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace WordpressAutomation
{
    public class ManageFamilyCodes
    {

        //Ingresar al ManageFamily en el menu
        public static void GotoManageFamily()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/ul/li[5]/a")).Click();
        }

        //Alert para verificar si abre a la ventana de ManageFamilhy
        public static bool AsertGotoManageFamily
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[5]/div/family-code-manager/div/div[1]/div/h3"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

        //Click al boton de AddNewFamilyCode

        public static void ClickButonAddNewFamilyCode()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[5]/div/family-code-manager/div/div[3]/div/a")).Click();
        }

        //Alert para verficar el click de FamilyCode
        public static bool AsertClickButonAddNewFamilyCode
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[6]/div/div/bs-modal-header/div/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //Combo box de Gs1 Prefix
        public static void AddGS1Prefix(string prefix)
        {

            Elements.ClickElementByXPath("//*[@id='gs1codeDropdownButton']");

            Elements.ClickElementByCssSelector("body > bs-modal.fade.modal.in > div > div > bs-modal-body > div > div > div:nth-child(1) > div > div > ul > li:nth-child(" + prefix + ") > a");

        }

        //Alert para   Gs1 Prefix

        public static bool AsertAddGS1Prefix
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[6]/div/div/bs-modal-body/div/div/div[2]/input"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }


        //Agregar Family Code
        public static void AddFamilyCode(string familycode)
        {

            Elements.SendKeysByXPath("/html/body/bs-modal[6]/div/div/bs-modal-body/div/form/div/div[1]/input", familycode);
        }


        //Asert para  Agregar Family Code
        public static bool AsertAddFamilyCode(string familycode)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[6]/div/div/bs-modal-body/div/form/div/div[1]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == familycode;
        }



        //Agregar Description
        public static void AddDescription(string description)
        {

            Elements.SendKeysByXPath("/html/body/bs-modal[6]/div/div/bs-modal-body/div/form/div/div[2]/input", description);
        }


        //Asert para  Agregar Description
        public static bool AsertAddDescription(string description)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[6]/div/div/bs-modal-body/div/form/div/div[2]/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == description;
        }


        //Button "OK" para crear el Family Code
        public static void ClickButtonAddFamilyCode()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[6]/div/div/bs-modal-footer/div/div/button[2]")).Click();
        }

        //Alert para verificar el el ok del button
        public static bool AsertClickButtonAddFamilyCode
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[5]/div/family-code-manager/div/div[1]"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }



        //*****************EDITAR FAMILY CODE****************************

        public static void GotoListFamilyCodes()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[5]/div/family-code-manager/div/div[4]/div/table/tbody/tr[5]/td[2]/a")).Click();
        }
       
        //Alert para verificar si abre a la ventana de EditarManageFamilhy
        public static bool AsertGotoListFamilyCodes
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[7]/div/div/bs-modal-header/div/h4"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }

        //Editar Family Code
        public static void EditFamilyCode(string familycode)
        {

            Elements.SendKeysByXPath("/html/body/bs-modal[7]/div/div/bs-modal-body/div/form/div/div[1]/div/input", familycode);
        }

        //Asert para  Editar Family Code
        public static bool AsertEditFamilyCode(string familycode)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[7]/div/div/bs-modal-body/div/form/div/div[1]/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == familycode;
        }



        //Editar Description
        public static void EditDescription(string description)
        {

            Elements.SendKeysByXPath("/html/body/bs-modal[7]/div/div/bs-modal-body/div/form/div/div[2]/div/input", description);
        }


        //Asert para  Editar  Description
        public static bool AsertEditDescription(string description)
        {
            IWebElement TargetElement = Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[7]/div/div/bs-modal-body/div/form/div/div[2]/div/input"));
            string getValue = TargetElement.GetAttribute("value");
            Console.WriteLine(getValue);
            return getValue == description;
        }


        //Button "OK" para Editar el Family Code
        public static void ClickButtonEditFamilyCode()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/bs-modal[7]/div/div/bs-modal-footer/div/div/button[2]")).Click();
        }

        //Alert para verificar el el ok del button
        public static bool AsertClickButtonEditFamilyCode
        {
            get
            {
                try
                {
                    Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[5]/div/family-code-manager/div/div[1]/div/h3"));

                }
                catch (NoSuchElementException e)
                {
                    return false;
                }
                return true;

            }
        }
    
    }
}

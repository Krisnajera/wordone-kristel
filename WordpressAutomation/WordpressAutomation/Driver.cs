﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WordpressAutomation
{
    public class Driver
    {

        public static IWebDriver Instance { get; set; }
      
        public static void Initialize()
        {
            Instance = new ChromeDriver();
            Instance.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(15);
        }

        public static void WaitForThePage(int timeToWait)
        {
            Thread.Sleep(timeToWait);
        }

        public static void Close()
        {
            Instance.Close();
        }

        public static string BaseAddress
        {
            get { return ""; }

        }
    }
}

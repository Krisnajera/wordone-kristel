﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;


namespace WordpressAutomation
{
  public class LoginPage
    {
        public static void GoTo()
        {
        //    Driver.Instance.Navigate().GoToUrl(Driver.BaseAddress +  "/login");
            Driver.Instance.Navigate().GoToUrl("");
        }
        

        public static LoginCommand LoginAs(string userName)
        {
            return new LoginCommand(userName);
        
        }
    }
        public class LoginCommand
        {
            private readonly string userName;
            private string password;


            public  LoginCommand(string userName)
            {
                  this.userName = userName;
                  //throw new NotImplementedException();
        
             }

            public LoginCommand WithPassword(string password)
            {
                this.password = password;
                return this;
            }

            public void Login()
            {
                var loginInput = Driver.Instance.FindElement(By.Id("username"));
                loginInput.SendKeys(userName);

                var passwordInput = Driver.Instance.FindElement(By.Id("password"));
                passwordInput.SendKeys(password);

                var loginButton = Driver.Instance.FindElement(By.CssSelector(".btn.btn-primary"));
                loginButton.Click();

                             
            }
        }
    }


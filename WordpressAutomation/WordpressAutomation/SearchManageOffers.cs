﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;


namespace WordpressAutomation
{
    public class SearchManageOffers
    {

        public static void GoToSearch()
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/summary-table/div/div/div[1]/div[1]/a/span")).Click();
          
        }

        public static void SearchByOfferCode(string offercode)
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/summary-table/div/div[1]/div[1]/div/input")).SendKeys(offercode);
         

        }
                    
          public static bool SearchHadSome
       {
           get
           {
               try
               {
                   Driver.Instance.FindElement(By.XPath("//table[@ng-reflect-input-data='[object Object]']"));
                       
               }
               catch (NoSuchElementException e)
               {
                   return false;
               }
               return true;

           }

       }
           
          
    
    //BUSQUEDA DE LAST ACTIVITY START

        public static void SearchByLastActivityStart(string lastactivitystart)
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/summary-table/div/div[1]/div[2]/div[1]/my-date-picker/div/div/input")).SendKeys(lastactivitystart);

        }

        //BUSQUEDA LAST ACTIVITY END

        public static void SearchByLastActivityEnd(string lastactivityend)
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/summary-table/div/div[1]/div[2]/div[2]/my-date-picker/div/div/input")).SendKeys(lastactivityend);
        }

        //BUSQUEDA MEDIATYPE COMBO-BOX

        public static void SearchByMediaType(string mediatype)
        {
        
            Elements.ClickElementByXPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/summary-table/div/div[1]/div[2]/div[3]/div/button");
            Elements.ClickElementByCssSelector("body > sea-app > offer-landing > tabs > tab:nth-child(2) > div > offer-search > div > summary-table > div > div:nth-child(1) > div:nth-child(3) > div.col-md-4 > div > ul > li:nth-child("  + mediatype + ") > a");
            
        }
        
        //BUSQUEDA DROP DATE START

        public static void SearchByDropStart(string dropstart)
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/summary-table/div/div[1]/div[2]/div[4]/my-date-picker/div/div/input")).SendKeys(dropstart);

        }

        //BUSQUEDA DROP DATE END

        public static void SearchByDropEnd(string dropend)
        {
            Driver.Instance.FindElement(By.XPath("/html/body/sea-app/offer-landing/tabs/tab[1]/div/offer-search/div/summary-table/div/div[1]/div[2]/div[5]/my-date-picker/div/div/input")).SendKeys(dropend);

        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordpressAutomation;

namespace WordpressTests
{
    [TestClass]
    public class ManageUserTest : WordpressTest
    {
        [TestMethod]
        public void AddnewUser()
        {
            ManageUsers.GotoManageUsers();
           Assert.IsTrue(ManageUsers.AsertGotoManageUsers, "Fail to AsertGotoManageUsers");

            ManageUsers.GotoUserCreation();
            Assert.IsTrue(ManageUsers.AsertGotoUserCreation, "Fail to AsertGotoUserCreation");

            Elements.Delay(3000);
            ManageUsers.AddFirstName("kristel");
            Assert.IsTrue(ManageUsers.AssertAddFirstName("kristel"), "Fail to AssertAddFirstName");

            Elements.Delay(3000);
            ManageUsers.AddLastName("gonzalez");
            Assert.IsTrue(ManageUsers.AssertAddLastName("gonzalez"), "Fail to AssertAddLastName");

            ManageUsers.AddPassword("12345");
            Assert.IsTrue(ManageUsers.AssertAddPassword("12345"), "Fail to AssertAddPassword");

            Elements.Delay(3000);
            ManageUsers.AddPasswordConfirm("12345");
            Assert.IsTrue(ManageUsers.AssertAddPasswordConfirm("12345"), "Fail to AssertAddPasswordConfirm");

            ManageUsers.AddEmail("kriss@najera.com");
            Assert.IsTrue(ManageUsers.AssertAddEmail("kriss@najera.com"), "Fail to AssertAddEmail");

            Elements.Delay(3000);
            ManageUsers.AddTypeAcount("4");
            Assert.IsTrue(ManageUsers.AssertAddTypeAcount, "Fail to AssertAddTypeAcount");

            Elements.Delay(3000);
            ManageUsers.ClickButtonSaveUser();
            Assert.IsTrue(ManageUsers.AsertClickButtonSaveUser, "Fail to ClickButtonSaveUser");

        }

           //////////********UPDATE USER

        [TestMethod]
        public void UpdateUser()
        {
            ManageUsers.GotoManageUsers();
            Assert.IsTrue(ManageUsers.AsertGotoManageUsers, "Fail to AsertGotoManageUsers");

            Elements.Delay(3000);
            ManageUsers.GotolistForupdate();
            Assert.IsTrue(ManageUsers.AsertGotolistForupdate, "Fail to AsertGotoUserForupdate");

            Elements.Delay(3000);
            ManageUsers.EditFirstName("kristelln");
            Assert.IsTrue(ManageUsers.AssertEditFirstName("kristelln"), "Fail to AssertEditFirstName");

            Elements.Delay(3000);
            ManageUsers.EditLastName("gonzalezz");
            Assert.IsTrue(ManageUsers.AssertEditLastName("gonzalezz"), "Fail to AssertEditLastName");

            Elements.Delay(3000);
            ManageUsers.EditTypeofAcount("1");
            Assert.IsTrue(ManageUsers.AssertEditTypeofAcount, "Fail to AssertAddTypeAcount");

            ManageUsers.GotoEditPassword();
            Assert.IsTrue(ManageUsers.AsertGotoEditPassword, "Fail to AsertGotoEditPassword");

            Elements.Delay(3000);
            ManageUsers.EditPassword("12346");
            Assert.IsTrue(ManageUsers.AssertEditPassword("12346"), "Fail to AssertEditPassword");

            Elements.Delay(3000);
            ManageUsers.EditPasswordConfirm("12346");
            Assert.IsTrue(ManageUsers.AssertEditPasswordConfirm("12346"), "Fail to AssertEditPasswordConfirm");


            Elements.Delay(3000);
            ManageUsers.ClickButtonYesEditpassword();
            Assert.IsTrue(ManageUsers.AsertClickButtonYesEditpassword, "Fail to AsertClickButtonYesEditpassword");

            Elements.Delay(3000);
            ManageUsers.SelectedAssignedClients("ABC Company");
            Assert.IsTrue(ManageUsers.AssertSelectedAssignedClients("ABC Company"), "Fail to AssertSelectedAssignedClients");

            Elements.Delay(3000);
            ManageUsers.ClickButtonUpdate();
            Assert.IsTrue(ManageUsers.AsertClickButtonUpdate, "Fail to AsertClickButtonUpdate");


            //////////********DELETE USER
        }
            
        [TestMethod]
        public void DeleteUser()
        {
            ManageUsers.GotoManageUsers();
            Assert.IsTrue(ManageUsers.AsertGotoManageUsers, "Fail to AsertGotoManageUsers");

            Elements.Delay(3000);
            ManageUsers.GotoManageUsers();
            Assert.IsTrue(ManageUsers.AsertGotoManageUsers, "Fail to AsertGotoManageUsers");

            Elements.Delay(3000);
            ManageUsers.GotolistFordelete();
            Assert.IsTrue(ManageUsers.AsertGotolistFordelete, "Fail to AsertGotoUserForupdate");

            Elements.Delay(3000);
            ManageUsers.ClickButtonEliminar();
            Assert.IsTrue(ManageUsers.AsertClickButtonEliminar, "Fail to AssertClickButtonEliminar");

            Elements.Delay(3000);
            ManageUsers.ClickConfirmDeleteUser();
            Assert.IsTrue(ManageUsers.AsertConfirmClickDeleteUser, "Fail to AssertClickButtonEliminar");
            
        }

    }
}

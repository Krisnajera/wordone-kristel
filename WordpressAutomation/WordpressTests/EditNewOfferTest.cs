﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordpressAutomation;

namespace WordpressTests
{
        [TestClass]
    public class EditNewOfferTest : WordpressTest
    {
       [TestMethod]
       public void EditOffer()
       { 
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("35");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            Elements.Delay(3000);
            AddNewOffer.AddOfferCode("111406");
            Assert.IsTrue(AddNewOffer.AssertAddOfferCode("111406"), "Fail to AssertAddOfferCode");

            AddNewOffer.AddOfferDescritpion("hello");
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion("hello"), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.AddFaceValue("0.02");
            Assert.IsTrue(AddNewOffer.AssertFaceValue("0.02"), "Fail to AssertFaceValue");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements("1");
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements("1"), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();

            Elements.Delay(3000);
            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            AddNewOffer.AddDropDate("08/17/2018");
            Assert.IsTrue(AddNewOffer.AssertDropDate("08/17/2018"), "fail to AssertDropDate");

            AddNewOffer.AddExpirationDate("08/08/2019");
            Assert.IsTrue(AddNewOffer.AssertExpirationDate("08/08/2019"), "Fail to AssertExpirationDate");


            AddNewOffer.Clickorderbarcode();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            AddNewOffer.Confirm();
            Elements.Delay(3000);
            Assert.IsTrue(AddNewOffer.AsertClickOrdenBarcocde, "Fail to ClickOrderBarcode");

            AddNewOffer.AddFiletype("1");
            Assert.IsTrue(AddNewOffer.AsertAddFiletype, "Fail to AssertAddFileType");

            AddNewOffer.AddEmailBarcode("krisnajera@hello.com");
            Assert.IsTrue(AddNewOffer.AssertAddEmailBarcode("krisnajera@hello.com"), "Fail to AssertAddEmailBarcode");

            AddNewOffer.AddSpecialInstructions();
            Assert.IsTrue(AddNewOffer.AssertAddSpecialInstructions, "Fail to AssertAddSpecial Instructions");

            //click al boton next
            Elements.Delay(3000);
            AddNewOffer.ClickButttonNext();
            Assert.IsTrue(AddNewOffer.AsertClickButtonNext, "Fail to AssertClickButtonNext");

            Elements.Delay(3000);
            AddNewOffer.ClickAcceptConditions();
            Assert.IsTrue(AddNewOffer.AssertClickAcceptConditions, "Fail to AssertClickConditions");

            Elements.Delay(3000);
            AddNewOffer.ButtonAceptConditions();
            Assert.IsTrue(AddNewOffer.AssertButtonAceptConditions, "Fail to AssertButtonAceptConditions");

            //Ingresar la informacion New Offer Marketing Information(Budgeting Information)

            Elements.Delay(3000);
            AddNewOffer.AddDistribution("55");
            Assert.IsTrue(AddNewOffer.AssertAddDistribution("55"), "Fail to AssertAddDistribution");
            Elements.Delay(3000);

            Elements.Delay(3000);
            AddNewOffer.AddEnterbudget("0.33");
            Assert.IsTrue(AddNewOffer.AssertAddEnterbudget("0.33"), "Fail to AssertAddEnterbudget");

            Elements.Delay(3000);
            AddNewOffer.AddCreativeCost("45");
            Assert.IsTrue(AddNewOffer.AssertCreativeCost("45"), "Fail to AssertCreativeCost");

            Elements.Delay(3000);
            AddNewOffer.AddPrintCost("21");
            Assert.IsTrue(AddNewOffer.AssertPrintCost("21"), "Fail to AssertPrintCost");

            //**Informacion Marketing Information dentro de la misma view de Budgeting Information

            Elements.Delay(3000);
            AddNewOffer.AddMediaType("15");
            Assert.IsTrue(AddNewOffer.AssertMediaType, "Fail to AsertMediaType");

            Elements.Delay(3000);
            AddNewOffer.AddEventDescription("descriptionasert");
            Assert.IsTrue(AddNewOffer.AssertEventDescription("descriptionasert"), "Fail to AssertPrintCost");

            //Buton para salvar
            Elements.Delay(3000);
            AddNewOffer.ClickButtonSave();
            Assert.IsTrue(AddNewOffer.AsertClickButtonSave, "Fail to AssertPrintCost");

            Elements.Delay(3000);
            EditNewOffer.ClickButtonEdit();
            Assert.IsTrue(EditNewOffer.AsertClickButtonEdit, "Fail to AsertClickButtonEdit");




           //**************************EDITAR  ******************************
            Elements.Delay(3000);
            EditNewOffer.EditOfferDescritpion("kris");
            Assert.IsTrue(EditNewOffer.AssertEditOfferDescripcion("kris"), "Fail to AssertAddOfferDescripcioon");

            Elements.Delay(3000);
            EditNewOffer.EditDropDate("07/21/2018");
            Assert.IsTrue(EditNewOffer.AssertEditDropDate("07/21/2018"), "fail to AssertDropDate");

            Elements.Delay(3000);
            EditNewOffer.EditDistribution("21");
            Assert.IsTrue(EditNewOffer.AssertEditDistribution("21"), "Fail to AssertAddDistribution");
            Elements.Delay(3000);

            Elements.Delay(3000);
            EditNewOffer.EditEnterbudget("0.21");
            Assert.IsTrue(EditNewOffer.AssertEditEnterbudget("0.21"), "Fail to AssertAddEnterbudget");

            Elements.Delay(3000);
            EditNewOffer.EditCreativeCost("21");
            Assert.IsTrue(EditNewOffer.AssertEditCreativeCost("21"), "Fail to AssertCreativeCost");

            Elements.Delay(3000);
            EditNewOffer.EditPrintCost("11");
            Assert.IsTrue(EditNewOffer.AssertEditPrintCost("11"), "Fail to AssertPrintCost");

            Elements.Delay(3000);
            EditNewOffer.EditMediaType("1");
            Assert.IsTrue(EditNewOffer.AssertEditMediaType, "Fail to AsertMediaType");

            Elements.Delay(3000);
            EditNewOffer.EditEventDescription("descriptionasertaaa");
            Assert.IsTrue(EditNewOffer.AssertEditEventDescription("descriptionasertaaa"), "Fail to AssertPrintCost");

            Elements.Delay(3000);
            EditNewOffer.ClickButtonSaveChanges();
            Assert.IsTrue(EditNewOffer.AsertClickButtonSaveChanges, "Fail to AsertClickButtonSaveChanges");

            Elements.Delay(3000);
            EditNewOffer.ClickButtonReturnManageOfers();
            Assert.IsTrue(EditNewOffer.AssertClickButtonReturnManageOfers, "Fail to AsertClickButtonSaveChanges");
       }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordpressAutomation;


namespace WordpressTests
{
    [TestClass]
   public class AddNewOfferTest:WordpressTest
    {

        /***************Test Case:Validate the positive entry data on the Add New Offer***************/
        // Insert code

        [TestMethod]
        public void AddnewOffer()
        {
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("5");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            AddNewOffer.AddOfferCode("111429");
            Assert.IsTrue(AddNewOffer.AssertAddOfferCode("111429"), "Fail to AssertAddOfferCode");

            AddNewOffer.AddOfferDescritpion("hello");
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion("hello"), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.AddFaceValue("0.02");
            Assert.IsTrue(AddNewOffer.AssertFaceValue("0.02"), "Fail to AssertFaceValue");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements("1");
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements("1"), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();

            Elements.Delay(3000);
            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            AddNewOffer.AddDropDate("08/17/2018");
            Assert.IsTrue(AddNewOffer.AssertDropDate("08/17/2018"), "fail to AssertDropDate");

            AddNewOffer.AddExpirationDate("08/08/2019");
            Assert.IsTrue(AddNewOffer.AssertExpirationDate("08/08/2019"), "Fail to AssertExpirationDate");
           

        }

        /***************Test Case:Validate the positive entry data on the Add New Offer***************/
        // With Auto Assign Next Available Code

        [TestMethod]
        public void AssignCodeButton()
        {
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("5");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            AddNewOffer.SelectAutoAssignCode();
            Assert.IsTrue(AddNewOffer.AssertSelectAutoAssignCode, "Fail to AssertSelectAutoAssignCode");

            AddNewOffer.AddOfferDescritpion("hello");
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion("hello"), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.AddFaceValue("0.02");
            Assert.IsTrue(AddNewOffer.AssertFaceValue("0.02"), "Fail to AssertFaceValue");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements("1");
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements("1"), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();

            Elements.Delay(3000);
            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            AddNewOffer.AddDropDate("08/17/2018");
            Assert.IsTrue(AddNewOffer.AssertDropDate("08/17/2018"), "fail to AssertDropDate");

            AddNewOffer.AddExpirationDate("08/08/2019");
            Assert.IsTrue(AddNewOffer.AssertExpirationDate("08/08/2019"), "Fail to AssertExpirationDate");
           

        }

        /***************Test Case:Validate the positive entry data on the Add New Offer***************/
        //Combo box selected si se selecciiona añade minimun y maximun
        [TestMethod]
        public void SelectedCombobox()
        {
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("5");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            AddNewOffer.SelectAutoAssignCode();
            Assert.IsTrue(AddNewOffer.AssertSelectAutoAssignCode, "Fail to AssertSelectAutoAssignCode");

            AddNewOffer.AddOfferDescritpion("hello");
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion("hello"), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.SelectFreeOfer();
            Assert.IsTrue(AddNewOffer.AssertSelectOffer(), "Fail to AssertSelectOffer");

            AddNewOffer.Addminimun("22.22");
            Assert.IsTrue(AddNewOffer.AssertAddMinimun("22.22"), "Fail to Assert AddMinimun");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.Addmaximun("55.55");
            Assert.IsTrue(AddNewOffer.AssertAddMaximum("55.55"), "Fail to AssertASddMaximun");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements("1");
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements("1"), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            Elements.Delay(3000);
            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            AddNewOffer.AddDropDate("08/17/2018");
            Assert.IsTrue(AddNewOffer.AssertDropDate("08/17/2018"), "fail to AssertDropDate");

            AddNewOffer.AddExpirationDate("08/08/2019");
            Assert.IsTrue(AddNewOffer.AssertExpirationDate("08/08/2019"), "Fail to AssertExpirationDate");
           


        }

        /***************Test Case:Validate the positive entry data on the Add New Offer***************/
        //Si se selecciona el checkbox de NoExpiration Date no se selecciona en la fecha

        [TestMethod]
        public void SelectNoExpirationDate()
        {
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("5");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            AddNewOffer.AddOfferCode("111429");
            Assert.IsTrue(AddNewOffer.AssertAddOfferCode("111429"), "Fail to AssertAddOfferCode");

            AddNewOffer.AddOfferDescritpion("hello");
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion("hello"), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.AddFaceValue("0.02");
            Assert.IsTrue(AddNewOffer.AssertFaceValue("0.02"), "Fail to AssertFaceValue");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements("1");
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements("1"), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

          
            AddNewOffer.SelectNoExpirationDate();
            Assert.IsTrue(AddNewOffer.AssertNoExpirationDate(), "fail to AssertNoExpirationDate");

            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            AddNewOffer.AddDropDate("08/17/2018");
            Assert.IsTrue(AddNewOffer.AssertDropDate("08/17/2018"), "fail to AssertDropDate");
   
        }


        //Test para Pedido de codigo de barras
        // ******************************** Order Barcode********************************************//
        [TestMethod]
        public void gotoOrderBarcode()
        {
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("5");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            AddNewOffer.AddOfferCode("111429");
            Assert.IsTrue(AddNewOffer.AssertAddOfferCode("111429"), "Fail to AssertAddOfferCode");

            AddNewOffer.AddOfferDescritpion("hello");
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion("hello"), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.AddFaceValue("0.02");
            Assert.IsTrue(AddNewOffer.AssertFaceValue("0.02"), "Fail to AssertFaceValue");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements("1");
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements("1"), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();

            Elements.Delay(3000);
            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            Elements.Delay(3000);
            AddNewOffer.AddDropDate("08/17/2018");
            Assert.IsTrue(AddNewOffer.AssertDropDate("08/17/2018"), "fail to AssertDropDate");

            AddNewOffer.AddExpirationDate("08/08/2019");
            Assert.IsTrue(AddNewOffer.AssertExpirationDate("08/08/2019"), "Fail to AssertExpirationDate");

            Elements.Delay(3000);
            AddNewOffer.Clickorderbarcode();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            AddNewOffer.Confirm();
            Elements.Delay(3000);
            Assert.IsTrue(AddNewOffer.AsertClickOrdenBarcocde, "Fail to ClickOrderBarcode");


            AddNewOffer.AddFiletype("1");
            Assert.IsTrue(AddNewOffer.AsertAddFiletype, "Fail to AssertAddFileType");

            AddNewOffer.AddEmailBarcode("krisnajera@hello.com");
            Assert.IsTrue(AddNewOffer.AssertAddEmailBarcode("krisnajera@hello.com"), "Fail to AssertAddEmailBarcode");

            AddNewOffer.AddSpecialInstructions();
            Assert.IsTrue(AddNewOffer.AssertAddSpecialInstructions, "Fail to AssertAddSpecial Instructions");

            //click al boton next
            AddNewOffer.ClickButttonNext();
            Assert.IsTrue(AddNewOffer.AsertClickButtonNext, "Fail to AssertClickButtonNext");

            Elements.Delay(3000);
            AddNewOffer.ClickAcceptConditions();
            Assert.IsTrue(AddNewOffer.AssertClickAcceptConditions, "Fail to AssertClickConditions");


            AddNewOffer.ButtonAceptConditions();
            Assert.IsTrue(AddNewOffer.AssertButtonAceptConditions, "Fail to AssertButtonAceptConditions");
        }

        //Test New Offer Marketing Information ya para salvar el codigo de barras
        // ******************************** Test New Offer Marketing Information********************************************//
        //Este test el checkbox aparece por defecto la opcion de  Calculate Budget by %

        [TestMethod]
        public void gotoMarketingInformation()
        {
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("35");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            AddNewOffer.AddOfferCode("111429");
            Assert.IsTrue(AddNewOffer.AssertAddOfferCode("111429"), "Fail to AssertAddOfferCode");

            AddNewOffer.AddOfferDescritpion("hello");
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion("hello"), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.AddFaceValue("0.02");
            Assert.IsTrue(AddNewOffer.AssertFaceValue("0.02"), "Fail to AssertFaceValue");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements("1");
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements("1"), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();

            Elements.Delay(3000);
            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            AddNewOffer.AddDropDate("08/17/2018");
            Assert.IsTrue(AddNewOffer.AssertDropDate("08/17/2018"), "fail to AssertDropDate");

            AddNewOffer.AddExpirationDate("08/08/2019");
            Assert.IsTrue(AddNewOffer.AssertExpirationDate("08/08/2019"), "Fail to AssertExpirationDate");


            AddNewOffer.Clickorderbarcode();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            AddNewOffer.Confirm();
            Elements.Delay(3000);
            Assert.IsTrue(AddNewOffer.AsertClickOrdenBarcocde, "Fail to ClickOrderBarcode");


            AddNewOffer.AddFiletype("1");
            Assert.IsTrue(AddNewOffer.AsertAddFiletype, "Fail to AssertAddFileType");

            AddNewOffer.AddEmailBarcode("krisnajera@hello.com");
            Assert.IsTrue(AddNewOffer.AssertAddEmailBarcode("krisnajera@hello.com"), "Fail to AssertAddEmailBarcode");

            AddNewOffer.AddSpecialInstructions();
            Assert.IsTrue(AddNewOffer.AssertAddSpecialInstructions, "Fail to AssertAddSpecial Instructions");

            //click al boton next
            Elements.Delay(3000);
            AddNewOffer.ClickButttonNext();
            Assert.IsTrue(AddNewOffer.AsertClickButtonNext, "Fail to AssertClickButtonNext");

            Elements.Delay(3000);
            AddNewOffer.ClickAcceptConditions();
            Assert.IsTrue(AddNewOffer.AssertClickAcceptConditions, "Fail to AssertClickConditions");

            Elements.Delay(3000);
            AddNewOffer.ButtonAceptConditions();
            Assert.IsTrue(AddNewOffer.AssertButtonAceptConditions, "Fail to AssertButtonAceptConditions");

            //Ingresar la informacion New Offer Marketing Information(Budgeting Information)

            Elements.Delay(3000);
            AddNewOffer.AddDistribution("55");
            Assert.IsTrue(AddNewOffer.AssertAddDistribution("55"), "Fail to AssertAddDistribution");
            Elements.Delay(3000);

            Elements.Delay(3000);
            AddNewOffer.AddEnterbudget("0.33");
            Assert.IsTrue(AddNewOffer.AssertAddEnterbudget("0.33"), "Fail to AssertAddEnterbudget");

            Elements.Delay(3000);
            AddNewOffer.AddCreativeCost("45");
            Assert.IsTrue(AddNewOffer.AssertCreativeCost("45"), "Fail to AssertCreativeCost");

            Elements.Delay(3000);
            AddNewOffer.AddPrintCost("21");
            Assert.IsTrue(AddNewOffer.AssertPrintCost("21"), "Fail to AssertPrintCost");

            //**Informacion Marketing Information dentro de la misma view de Budgeting Information

            Elements.Delay(3000);
            AddNewOffer.AddMediaType("15");
            Assert.IsTrue(AddNewOffer.AssertMediaType, "Fail to AsertMediaType");

            Elements.Delay(3000);
            AddNewOffer.AddEventDescription("descriptionasert");
            Assert.IsTrue(AddNewOffer.AssertEventDescription("descriptionasert"), "Fail to AssertPrintCost");

            //Buton para salvar
            AddNewOffer.ClickButtonSave();
            Assert.IsTrue(AddNewOffer.AsertClickButtonSave, "Fail to AssertPrintCost");

            
        }


        //Test New Offer Marketing Information ya para salvar el codigo de barras
        // ******************************** Test New Offer Marketing Information********************************************//
        //Este test cambia el checkbox por Calculate Budget by $
        //revisar
        /*
        [TestMethod]
        public void gotoMarketingInformationchangeCombobox()
        {
            AddNewOffer.GotoAdd();
            AddNewOffer.AddGS1Prefix("35");
            Assert.IsTrue(AddNewOffer.SearchGS1Prefix, "Fail to SearchGS1Prefix");

            AddNewOffer.AddOfferCode();
            Assert.IsTrue(AddNewOffer.AssertAddOfferCode(), "Fail to AssertAddOfferCode");

            AddNewOffer.AddOfferDescritpion();
            Assert.IsTrue(AddNewOffer.AssertAddOfferDescripcion(), "Fail to AssertAddOfferDescripcioon");

            AddNewOffer.AddFaceValue();
            Assert.IsTrue(AddNewOffer.AssertFaceValue(), "Fail to AssertFaceValue");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();
            Elements.Delay(3000);

            AddNewOffer.AddPurchaseRequirements();
            Assert.IsTrue(AddNewOffer.AssertPurchaseRequirements(), "Fail to PurchaseRequirements");
            AddNewOffer.Click();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            Elements.Delay(3000);
            AddNewOffer.Confirm();

            Elements.Delay(3000);
            AddNewOffer.SelectFamilyCode("1");
            Assert.IsTrue(AddNewOffer.AssertFamilyCode, "Fail to AssertFamilyCode");

            Elements.Delay(3000);
            AddNewOffer.AddDropDate();
            Assert.IsTrue(AddNewOffer.AssertDropDate(), "fail to AssertDropDate");

            AddNewOffer.AddExpirationDate();
            Assert.IsTrue(AddNewOffer.AssertExpirationDate(), "Fail to AssertExpirationDate");


            AddNewOffer.Clickorderbarcode();
            Assert.IsTrue(AddNewOffer.AsertConfirm, "Fail to Confirm");
            AddNewOffer.Confirm();
            Elements.Delay(3000);
            Assert.IsTrue(AddNewOffer.AsertClickOrdenBarcocde, "Fail to ClickOrderBarcode");


            AddNewOffer.AddFiletype("1");
            Assert.IsTrue(AddNewOffer.AsertAddFiletype, "Fail to AssertAddFileType");

            AddNewOffer.AddEmailBarcode();
            Assert.IsTrue(AddNewOffer.AssertAddEmailBarcode(), "Fail to AssertAddEmailBarcode");

            AddNewOffer.AddSpecialInstructions();
            Assert.IsTrue(AddNewOffer.AssertAddSpecialInstructions, "Fail to AssertAddSpecial Instructions");

            //click al boton next
            Elements.Delay(3000);
            AddNewOffer.ClickButttonNext();
            Assert.IsTrue(AddNewOffer.AsertClickButtonNext, "Fail to AssertClickButtonNext");

            Elements.Delay(3000);
            AddNewOffer.ClickAcceptConditions();
            Assert.IsTrue(AddNewOffer.AssertClickAcceptConditions, "Fail to AssertClickConditions");

            Elements.Delay(3000);
            AddNewOffer.ButtonAceptConditions();
            Assert.IsTrue(AddNewOffer.AssertButtonAceptConditions, "Fail to AssertButtonAceptConditions");

            //Ingresar la informacion New Offer Marketing Information(Budgeting Information)

            Elements.Delay(3000);
            AddNewOffer.AddDistribution();
            Assert.IsTrue(AddNewOffer.AssertAddDistribution(), "Fail to AssertAddDistribution");
            Elements.Delay(3000);

            //Seleccionar el checkbox de porcentaje
            Elements.Delay(3000);
            AddNewOffer.SelectCalBudgetdollars();
            Assert.IsTrue(AddNewOffer.AssertSelectCalBudgetdollars(), "Fail to AssertSelectCalBudgetpercentage");

            Elements.Delay(3000);
            AddNewOffer.AddEnterbudget();
            Assert.IsTrue(AddNewOffer.AssertAddEnterbudget(), "Fail to AssertAddEnterbudget");

            Elements.Delay(3000);
            AddNewOffer.AddCreativeCost();
            Assert.IsTrue(AddNewOffer.AssertCreativeCost(), "Fail to AssertCreativeCost");

            Elements.Delay(3000);
            AddNewOffer.AddPrintCost();
            Assert.IsTrue(AddNewOffer.AssertPrintCost(), "Fail to AssertPrintCost");

            //**Informacion Marketing Information dentro de la misma view de Budgeting Information

            Elements.Delay(3000);
            AddNewOffer.AddMediaType("15");
            Assert.IsTrue(AddNewOffer.AssertMediaType, "Fail to AsertMediaType");

            Elements.Delay(3000);
            AddNewOffer.AddEventDescription();
            Assert.IsTrue(AddNewOffer.AssertEventDescription(), "Fail to AssertPrintCost");

            //Buton para salvar
            AddNewOffer.ClickButtonSave();
            Assert.IsTrue(AddNewOffer.AsertClickButtonSave, "Fail to AssertPrintCost");

        }

        */
    }
}


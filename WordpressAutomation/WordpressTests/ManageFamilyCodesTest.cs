﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordpressAutomation;

namespace WordpressTests
{
      [TestClass]
    public class ManageFamilyCodesTest : WordpressTest
    {
          [TestMethod]
          public void AddManageFamily()
          {

              ManageFamilyCodes.GotoManageFamily();
              Assert.IsTrue(ManageFamilyCodes.AsertGotoManageFamily, "Fail to AsertGotoManageFamily");


              ManageFamilyCodes.ClickButonAddNewFamilyCode();
              Assert.IsTrue(ManageFamilyCodes.AsertClickButonAddNewFamilyCode, "Fail to AsertClickButonAddNewFamilyCode");

              Elements.Delay(3000);
              ManageFamilyCodes.AddGS1Prefix("2");
              Assert.IsTrue(ManageFamilyCodes.AsertAddGS1Prefix, "Fail to AsertAddGS1Prefix");

              Elements.Delay(3000);
              ManageFamilyCodes.AddFamilyCode("031");
              Assert.IsTrue(ManageFamilyCodes.AsertAddFamilyCode("031"), "Fail to AsertAddGS1Prefix");

              Elements.Delay(3000);
              ManageFamilyCodes.AddDescription("test5");
              Assert.IsTrue(ManageFamilyCodes.AsertAddDescription("test5"), "Fail to AddDescription");

              Elements.Delay(3000);
              ManageFamilyCodes.ClickButtonAddFamilyCode();
              Assert.IsTrue(ManageFamilyCodes.AsertClickButtonAddFamilyCode, "Fail to AddDescription");

          }

        //*****************EDITAR FAMILY CODE****************************

          [TestMethod]
          public void EditFamilyCode()
          {

              ManageFamilyCodes.GotoManageFamily();
              Assert.IsTrue(ManageFamilyCodes.AsertGotoManageFamily, "Fail to AsertGotoManageFamily");

              Elements.Delay(3000);
              ManageFamilyCodes.EditFamilyCode("111");
              Assert.IsTrue(ManageFamilyCodes.AsertEditFamilyCode("111"), "Fail to AsertAddGS1Prefix");

              Elements.Delay(3000);
              ManageFamilyCodes.EditDescription("test111");
              Assert.IsTrue(ManageFamilyCodes.AsertEditDescription("test111"), "Fail to AddDescription");

              Elements.Delay(3000);
              ManageFamilyCodes.ClickButtonEditFamilyCode();
              Assert.IsTrue(ManageFamilyCodes.AsertClickButtonEditFamilyCode, "Fail to AddDescription");
          }
    }
}
      
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordpressAutomation;

namespace WordpressTests
{
    [TestClass]
    public class SearchManageOfferTest : WordpressTest
    {
        //test para buscar por OFFERCODE
        [TestMethod]
        public void SearchByOfferCode()
        {
               SearchManageOffers.GoToSearch();
               SearchManageOffers.SearchByOfferCode("123456");
               Assert.IsTrue(SearchManageOffers.SearchHadSome, "No records match the parameters of this search. Please try again with different parameters");
        }

        //test para buscar por LASTACTIVITYSTART
          [TestMethod]
        public void SearchByLastActivityStar() 
        {
            SearchManageOffers.GoToSearch();
            SearchManageOffers.SearchByLastActivityStart("07/02/2018");
            Assert.IsTrue(SearchManageOffers.SearchHadSome, "No records match the parameters of this search. Please try again with different parameters");
        }

          //test para buscar por LASTACTIVITYEND
         [TestMethod]
        public void LastActivityEnd() 
        {
            SearchManageOffers.GoToSearch();
            SearchManageOffers.SearchByLastActivityEnd("08/02/2018");
            Assert.IsTrue(SearchManageOffers.SearchHadSome, "No records match the parameters of this search. Please try again with different parameters");
        }

         //test para buscar por MEDIATYPE
           [TestMethod]
        public void MediaType() 
        {
         SearchManageOffers.GoToSearch();
         SearchManageOffers.SearchByMediaType("1");
         Assert.IsTrue(SearchManageOffers.SearchHadSome, "No records match the parameters of this search. Please try again with different parameters");
        
        }

           //test para buscar por DROPSTART
           [TestMethod]
        public void DropStart()
        {
            SearchManageOffers.GoToSearch();
            SearchManageOffers.SearchByDropStart("08/07/2018");
            Assert.IsTrue(SearchManageOffers.SearchHadSome, "No records match the parameters of this search. Please try again with different parameters");
        }

        //test para buscar por DROPEND
           [TestMethod]
        public void DropEnd() 
        {
            SearchManageOffers.GoToSearch();
            SearchManageOffers.SearchByDropEnd("08/07/2018");
            Assert.IsTrue(SearchManageOffers.SearchHadSome, "No records match the parameters of this search. Please try again with different parameters");


        }

    }

}
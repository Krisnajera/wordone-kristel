1)Cuales fueron las localidades con menos reservaciones


Select count(Codlocalidad)as total,nombrelocalidad
from Hotel
inner join Localidad
on  Hotel.Codlocalidad = Localidad.codlocal
group by Codlocalidad,nombrelocalidad
order by COUNT(Codlocalidad)asc


2)Cual fue el tipo de habitacion mas reservada por los clientes

Select count(Habitacion.codtipohabitacion)as total,[Tipo de Habitacion].descripcion 
from Habitacion
inner join [Tipo de Habitacion]
on  Habitacion.codtipohabitacion = [Tipo de Habitacion].codtipohabitacion
group by Habitacion.codtipohabitacion,[Tipo de Habitacion].descripcion
order by count(Habitacion.codtipohabitacion)desc


3)Los  hoteles con el precio mas caro en el pais de Costa Rica

Select Top(5)R.preciototal,Hotel.nombrehotel from Reservaciones R,Hotel
where R.codhotel=Hotel.codhotel and Hotel.codpais=1
group by R.preciototal,Hotel.nombrehotel
order by R.preciototal desc 


4)Hoteles mas reservados por mes
Select Top(5)Hotel.nombrehotel,Reservaciones.mes,COUNT(Reservaciones.codhotel) as cantidad
 from Hotel
inner join Reservaciones
on  Hotel.codhotel=Reservaciones.codhotel
group by Hotel.nombrehotel,Reservaciones.mes 
order by count(Hotel.codhotel)desc


5)Las  atracciones mas reservadas
Select Top(5)(Atracciones.CodAtraccion) as solicitadas, Atracciones.Descripcion as Descripcion from Atracciones ,Reservaciones b
where Atracciones.CodAtraccion=b.codhotel
group by Atracciones.CodAtraccion,Atracciones.Descripcion
order by solicitadas desc


6)Los  paises con mayor reservaciones

Select (Reservaciones.coddirpago)as reservas,[Direccion Pago].pais
from Reservaciones
inner join [Direccion Pago]
on  Reservaciones.coddirpago = [Direccion Pago].coddirpago
group by Reservaciones.coddirpago,[Direccion Pago].pais
order by(Reservaciones.coddirpago)desc


7)Los usuarios con mayor reservaciones

Select Clientes.nombre,COUNT([Resevacion por Usuarios].codusuario) as Reservaciones from Clientes,[Resevacion por Usuarios]
where Clientes.codusuario=[Resevacion por Usuarios].codusuario
group by Clientes.nombre
order by COUNT([Resevacion por Usuarios].codusuario) desc


8)Hotel con mejor calificacion


select Hotel.nombrehotel,Hotel.calificacion as Calificacion from Hotel
group by nombrehotel,Hotel.calificacion
order by Hotel.calificacion desc



9)Los servicios mas solicitados por los clientes
Select Top(5)(Servicios.CodServicios) as solicitadas,Servicios.Descripcion from Servicios,[Atraccion por hotel],[Resevacion por Usuarios]
where Servicios.CodServicios=[Resevacion por usuarios].codhotel
group by  Servicios.CodServicios,Servicios.Descripcion 
order by (Servicios .CodServicios) desc


10)Cuantos hoteles por localidad

Select count(Hotel.Codlocalidad)as total,Localidad.nombrelocalidad
from Hotel
inner join Localidad
on  Hotel.Codlocalidad = Localidad.codlocal
group by  Hotel.Codlocalidad,Localidad.nombrelocalidad
order by count (Hotel.Codlocalidad)desc



